## Getting started

# flightradar24-client-ts

[![npm package][npm-img]][npm-url]
[![Downloads][downloads-img]][downloads-url]
[![semantic-release][semantic-release-img]][semantic-release-url]
[![Commitizen friendly][commitizen-img]][commitizen-url]

## Install

```bash
npm install flightradar24-client-ts
```

## Usage

### Basic Usage

```ts
import { FlightRadarApi } from "flightradar24-client-ts";

const flightRadarApi = new FlightRadarApi()
// airports
const airports = await api.fetchAirports();
// airlines
const airlines = await api.fetchAirlines();
// details of a single flight
const flightDetail = await api.fetchFlight("<flight code (ICAO or IATA)>")
// Radar
// get major zones
const zones = await api.fetchZones();
// get flights in a zone
const flights = await api.fetchFromRadar({
  zone: zones[0]
});
// multi-zone flight fetch
const multizoneFlights = await api.fetchFromRadarMultiZone(zones);
```

### Filtering live flights

```ts
import { FlightRadarApi } from "flightradar24-client-ts";

const flightRadarApi = new FlightRadarApi();

interface LiveFilters {
  /**
   * The airline ICAO code to fetch aircraft from (e.g. AFR for Air France)
   */
  airline?: string[];
  /**
   * The call signs to fetch aircraft from (e.g.SFR850)
   */
  callsign?: string[];
  /**
   * The flight numbers to fetch aircraft from (e.g. FA850)
   */
  flight?: string[];
  /**
   * The aircraft registration numbers to fetch aircraft from (e.g. F-GZNP)
   */
  reg?: string[];
  /**
   * The aircraft model codes to fetch aircraft from (e.g. A320)
   */
  type?: string[];
  /**
   * The Airports ICAO codes to fetch aircraft from (e.g. LFPG)
   */
  airport?: string[];
}

export interface RadarOptions {
  /**
   * The zone to fetch aircraft from. If not specified, the entire world is used.
   */
  zone?: ZoneData;
  /**
   * Whether to fetch inactive aircraft
   */
  inactive?: boolean;
  /**
   * Whether to fetch aircraft on ground
   */
  onGround?: boolean;
  /**
   * Whether to fetch gliders
   */
  gliders?: boolean;
  /**
   * Whether to fetch aircraft from the MLAT data source
   */
  MLAT?: boolean;
  /**
   * Whether to fetch aircraft from the FAA data source
   */
  FAA?: boolean;

  /**
   * Whether to fetch aircraft from the satellite data source
   */
  satellite?: boolean;
  /**
   * Whether to fetch vehicles
   */
  vehicles?: boolean;
  /**
   * Whether to fetch estimated positions
   */
  estimatedPositions?: boolean;
  /**
   * Whether to fetch aircraft from the FLARM data source
   */
  FLARM?: boolean;
  /**
   * Whether to fetch aircraft from the ADS-B data source
   */
  ADSB?: boolean;
  /**
   * Whether to fetch airborne aircraft
   */
  inAir?: boolean;
  /**
   * The filters to apply to the aircraft data
   */
  filters?: LiveFilters;
}


const radarOptions: RadarOptions = {
  // Filters
};
const flights = flightRadarApi.fetchFromRadar({
  radarOptions
});
```

- Example: Filtering Flights related to JFK Airport

```ts
import { defaultRadarOptions, FlightRadarApi } from "flightradar24-client-ts";

const flightRadarApi = new FlightRadarApi();
const flightsJFK = flightRadarApi.fetchFromRadar({
  ...defaultRadarOptions,
  filters: {
    airport: ["JFK"]
  }
});
```

## API

Api docs can be found [here](https://dev6645326.gitlab.io/flightradar24-client-ts/classes/FlightRadarApi.html)

## Example Project

### Flight Tracker

![img.png](https://gitlab.com/dev6645326/flightradar24-client-ts/-/raw/master/readme_assets/flight-tracker-example-project.png)
This project uses the flightradar24-client-ts to fetch flights from the radar and display them on a 3d globe. It can
also filter the flights based on the user's input.

- link to the project: https://gitlab.com/dev6645326/react-flight-tracker
- link to the live demo: https://react-flight-tracker.apoorva64.com/

[build-img]:https://gitlab.com/dev6645326/flightradar24-client-ts/badges/master/pipeline.svg

[build-url]:https://gitlab.com/dev6645326/flightradar24-client-ts/-/pipelines

[downloads-img]:https://img.shields.io/npm/dt/flightradar24-client-ts

[downloads-url]:https://www.npmtrends.com/flightradar24-client-ts

[npm-img]:https://img.shields.io/npm/v/flightradar24-client-ts

[npm-url]:https://www.npmjs.com/package/flightradar24-client-ts

[issues-img]:https://img.shields.io/gitlab.com/dev6645326/flightradar24-client-ts/-/issues

[issues-url]:https://gitlab.com/dev6645326/flightradar24-client-ts/-/issues

[codecov-img]:https://codecov.io/gh/ryansonshine/flightradar24-client-ts/branch/main/graph/badge.svg

[codecov-url]:https://codecov.io/gh/ryansonshine/flightradar24-client-ts

[semantic-release-img]:https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg

[semantic-release-url]:https://github.com/semantic-release/semantic-release

[commitizen-img]:https://img.shields.io/badge/commitizen-friendly-brightgreen.svg

[commitizen-url]:http://commitizen.github.io/cz-cli/
