import { FlightRadarApi } from '../src';
import { defaultRadarOptions } from '../src/config';

describe('index', () => {
  let api = new FlightRadarApi();

  beforeEach(() => {
    api = new FlightRadarApi();
  });

  describe('fetch flight', () => {
    it('should return a flight detail', async () => {
      const flights = await api.fetchFromRadar();
      const flightDetail = await api.fetchFlight(flights[0].id);
      expect(flights).toBeDefined();
      expect(flightDetail).toBeDefined();
      // save to file
    });
  });

  describe('airports API', () => {
    it('should return a list of airports', async () => {
      const airports = await api.fetchAirports();
      expect(
        airports.filter(
          airport => airport.iata === 'JFK' || airport.icao === 'KJFK'
        )
      ).toHaveLength(1);
      expect(airports).toBeDefined();
    });
    it('should return Airport details', async () => {
      const airport = await api.fetchAirport('JFK');
      expect(airport).toBeDefined();
      expect(airport.airport.pluginData.details.code.iata).toBe('JFK');
    });
  });

  describe('airlines API', () => {
    it('should return a list of airlines', async () => {
      const airlines = await api.fetchAirlines();
      expect(airlines).toBeDefined();
    });
  });

  describe('zones API', () => {
    it('should return a list of zones', async () => {
      const zones = await api.fetchZones();
      expect(zones).toBeDefined();
    });
  });

  describe('fetch live data', () => {
    it('should return fights from JFK', async function () {
      const flights = await api.fetchFromRadar({
        ...defaultRadarOptions,
        filters: {
          airport: ['JFK'],
        },
      });
      expect(flights).toBeDefined();
      for (const flight of flights) {
        expect([flight.origin, flight.destination]).toContain('JFK');
      }
    });
  });
});
