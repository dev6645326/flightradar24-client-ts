import querystring from 'querystringify';
import {
  airlines_data_url,
  airport_data_url,
  airports_data_url,
  flight_data_url,
  real_time_flight_tracker_data_url,
  zones_data_url,
} from './endpoints';
import {
  AircraftData,
  AirlineDetail,
  DictZoneData,
  Flight,
  ZoneData,
} from './types';
import fetch from 'cross-fetch';
import {
  AirportData,
  AirportDetailData,
  AirportDetailResponse,
} from './airportTypes';
import {
  defaultFlightRadarApiConfig,
  defaultRadarOptions,
  FlightRadarApiConfig,
  RadarOptions,
} from './config';

export class FlightRadarApi {
  private config: FlightRadarApiConfig;

  /**
   * Creates a new instance of the FlightRadarApi class.
   * @param config The configuration to use for the API. The default configuration is used if not specified({@link defaultFlightRadarApiConfig}).
   */
  constructor(config: FlightRadarApiConfig = defaultFlightRadarApiConfig) {
    this.config = config;
  }

  /**
   * Makes an API request to the FlightRadar24 API and returns the result.
   * If the request fails, an error is thrown.
   * If the request succeeds, the result is returned in JSON format.
   *
   * @Remarks
   * This method is used internally by the library and is not intended to be used directly.
   *
   * @returns The result of the API request.
   *
   * @param url The URL to make the request to.
   * @param options The options to pass to the fetch request.
   */
  async apiRequest(
    url: string,
    options: RequestInit = {}
  ): Promise<Record<string, any>> {
    const res = await fetch(
      this.config.corsProxy ? this.config.corsProxy + url : url,
      {
        ...this.config.fetchOptions?.(),
        ...options,
      }
    );
    if (!res.ok) {
      throw new Error(res.statusText);
    }
    return (await res.json()) as Record<string, any>;
  }

  /**
   * Fetches detailed information about a flight from the FlightRadar24 API.
   * @param flight The id of the flight to fetch.
   * @returns The flight data.
   */
  async fetchFlight(flight: string): Promise<Flight> {
    const url = `${flight_data_url}?${querystring.stringify({
      flight,
      version: '1.5',
    })}`;
    return (await this.apiRequest(url)) as Flight;
  }

  /**
   * Fetches detailed information about an Airport from the FlightRadar24 API.
   * @param code The ICAO or IATA code of the airport to fetch.
   * @param page Page number of the results to fetch (arrivals/departures).
   * @returns The airport data.
   */
  async fetchAirport(
    code: string,
    page: number = 1
  ): Promise<AirportDetailData> {
    const url = `${airport_data_url}?code=${code}&plugin[]=details&plugin[]=runways&plugin[]=scheduledRoutesStatistics&plugin[]=weather&plugin[]=schedule&page=${page}`;
    return ((await this.apiRequest(url)) as AirportDetailResponse).result
      .response;
  }

  /**
   * Fetches airports data from the FlightRadar24 API.
   *
   * @Remarks The number of airports returned may be less than the limit.
   * @returns The airports data.
   */
  async fetchAirports(): Promise<AirportData[]> {
    const url = `${airports_data_url}`;
    return (await this.apiRequest(url))['rows'] as AirportData[];
  }

  /**
   * Fetches the airlines data from the FlightRadar24 API.
   * @returns The airlines data.
   */
  async fetchAirlines(): Promise<AirlineDetail[]> {
    return (await this.apiRequest(airlines_data_url))[
      'rows'
    ] as AirlineDetail[];
  }

  /**
   * Fetches the zones data from the FlightRadar24 API.
   * The zones data contains the coordinates of the zones and subzones.
   * @remarks
   * The zones data is used to fetch aircraft from a specific zone.
   * @returns The zones data.
   */
  async fetchZones(): Promise<ZoneData[]> {
    const data: DictZoneData = (await this.apiRequest(
      zones_data_url
    )) as DictZoneData;
    delete data['version'];
    const zones: ZoneData[] = [];
    Object.entries(data).forEach(([key, value]) => {
      zones.push({
        name: key,
        tl_y: value.tl_y,
        br_y: value.br_y,
        tl_x: value.tl_x,
        br_x: value.br_x,
        subzones: value.subzones
          ? Object.entries(value.subzones).map(([key, value]) => {
              return {
                name: key,
                tl_y: value.tl_y,
                br_y: value.br_y,
                tl_x: value.tl_x,
                br_x: value.br_x,
              };
            })
          : undefined,
      });
    });
    return zones;
  }

  /**
   * Fetches live aircraft data from the FlightRadar24 API.
   *
   * @remarks
   * The data returned is limited to 1500 aircraft.
   * @param options The options to pass to the API request.
   */
  async fetchFromRadar(
    options: RadarOptions = defaultRadarOptions
  ): Promise<AircraftData[]> {
    const query: Record<string, any> = {
      // options
      faa: options.FAA ? '1' : '0',
      flarm: options.FLARM ? '1' : '0',
      mlat: options.MLAT ? '1' : '0',
      adsb: options.ADSB ? '1' : '0',
      satellite: options.satellite ? '1' : '0',
      air: options.inAir ? '1' : '0',
      gnd: options.onGround ? '1' : '0',
      vehicles: options.inactive ? '1' : '0',
      gliders: options.gliders ? '1' : '0',
      estimated: options.estimatedPositions ? '1' : '0',
      ...options.filters,
    };
    if (options.zone) {
      query[
        'bounds'
      ] = `${options.zone.tl_y},${options.zone.br_y},${options.zone.tl_x},${options.zone.br_x}`;
    }

    const url =
      real_time_flight_tracker_data_url + querystring.stringify(query, '?');

    const data = (await this.apiRequest(url)) as { [key: string]: any[] };
    const aircrafts: AircraftData[] = [];
    for (const id in data) {
      const d = data[id];
      if (!Array.isArray(d)) continue;
      aircrafts.push({
        id,
        modeSCode: d[0] ?? null, // ICAO aircraft registration number
        trailEntity: {
          lat: d[1],
          lng: d[2],
          hd: d[3], // in degrees
          alt: d[4], // in feet
          spd: d[5] ?? null, // in knots
          ts: d[10] ?? null, // timestamp
        },
        aircraft: {
          model: {
            code: d[8] ?? null, // ICAO aircraft type designator
          },
          registration: d[9] ?? null,
        },
        flight: d[13] ?? null,
        callsign: d[16] ?? null, // ICAO ATC call signature
        origin: d[11] ?? null, // airport IATA code
        destination: d[12] ?? null, // airport IATA code
        rateOfClimb: d[15], // ft/min
        isOnGround: !!d[14],
        squawkCode: d[6], // https://en.wikipedia.org/wiki/Transponder_(aeronautics)
        radar: d[7], // F24 "radar" data source ID
        isGlider: !!d[17],
      });
    }

    return aircrafts;
  }

  /**
   * Fetches live aircraft data from the FlightRadar24 API from multiple zones.
   * As the API only allows fetching 1500 aircraft at a time, this method fetches aircraft from multiple zones and combines the results.
   * @Remarks Be careful when using this method as it may result in a lot of API requests.
   * @param zones The zones to fetch aircraft from.
   * @param options The options to pass to the API request.
   */
  async fetchFromRadarMultiZone(
    zones: ZoneData[],
    options: RadarOptions = defaultRadarOptions
  ): Promise<AircraftData[]> {
    const promises: Promise<AircraftData[]>[] = [];
    const aircraft = new Set<AircraftData>();
    for (const zone of zones) {
      promises.push(this.fetchFromRadar({ ...options, zone }));
    }
    const aircrafts: AircraftData[][] = await Promise.all(promises);
    aircrafts.forEach(a => {
      a.forEach(b => {
        aircraft.add(b);
      });
    });
    return Array.from(aircraft);
  }
}
