export const api_flightradar_base_url =
  'https://api.flightradar24.com/common/v1';
export const cdn_flightradar_base_url = 'https://cdn.flightradar24.com';
export const flightradar_base_url = 'https://www.flightradar24.com';
export const data_live_base_url = 'https://data-live.flightradar24.com';
export const data_cloud_base_url = 'https://data-cloud.flightradar24.com';
export const user_login_url = flightradar_base_url + '/user/login';
export const user_logout_url = flightradar_base_url + '/user/logout';
export const real_time_flight_tracker_data_url =
  data_cloud_base_url + '/zones/fcgi/feed.js';
export const flight_data_url = data_live_base_url + '/clickhandler/';
export const api_airport_data_url = api_flightradar_base_url + '/airport.json';
export const airport_data_url = api_flightradar_base_url + '/airport.json';
export const airports_data_url = flightradar_base_url + '/_json/airports.php';
export const airlines_data_url = flightradar_base_url + '/_json/airlines.php';
export const zones_data_url = flightradar_base_url + '/js/zones.js.php';
export const country_flag_url =
  flightradar_base_url + '/static/images/data/flags-small/{}.svg';

export const airline_logo_url =
  cdn_flightradar_base_url + '/assets/airlines/logotypes/{}_{}.png';
export const alternative_airline_logo_url =
  flightradar_base_url + '/static/images/data/operators/{}_logo0.png';
