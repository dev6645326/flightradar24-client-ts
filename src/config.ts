import { getFetchOptions } from './utils';
import { ZoneData } from './types';

export interface FlightRadarApiConfig {
  corsProxy?: string;
  fetchOptions?: () => RequestInit;
}

/**
 * The default FlightRadarApiConfig
 */
export const defaultFlightRadarApiConfig: FlightRadarApiConfig = {
  corsProxy: 'https://corsproxy.io/?',
  fetchOptions: getFetchOptions,
};

export interface LiveFilters {
  /**
   * The airline ICAO code to fetch aircraft from (e.g. AFR for Air France)
   */
  airline?: string[];
  /**
   * The call signs to fetch aircraft from (e.g.SFR850)
   */
  callsign?: string[];
  /**
   * The flight numbers to fetch aircraft from (e.g. FA850)
   */
  flight?: string[];
  /**
   * The aircraft registration numbers to fetch aircraft from (e.g. F-GZNP)
   */
  reg?: string[];
  /**
   * The aircraft model codes to fetch aircraft from (e.g. A320)
   */
  type?: string[];
  /**
   * The Airports ICAO codes to fetch aircraft from (e.g. LFPG)
   */
  airport?: string[];
}

export interface RadarOptions {
  /**
   * The zone to fetch aircraft from. If not specified, the entire world is used.
   */
  zone?: ZoneData;
  /**
   * Whether to fetch inactive aircraft
   */
  inactive?: boolean;
  /**
   * Whether to fetch aircraft on ground
   */
  onGround?: boolean;
  /**
   * Whether to fetch gliders
   */
  gliders?: boolean;
  /**
   * Whether to fetch aircraft from the MLAT data source
   */
  MLAT?: boolean;
  /**
   * Whether to fetch aircraft from the FAA data source
   */
  FAA?: boolean;

  /**
   * Whether to fetch aircraft from the satellite data source
   */
  satellite?: boolean;
  /**
   * Whether to fetch vehicles
   */
  vehicles?: boolean;
  /**
   * Whether to fetch estimated positions
   */
  estimatedPositions?: boolean;
  /**
   * Whether to fetch aircraft from the FLARM data source
   */
  FLARM?: boolean;
  /**
   * Whether to fetch aircraft from the ADS-B data source
   */
  ADSB?: boolean;
  /**
   * Whether to fetch airborne aircraft
   */
  inAir?: boolean;
  /**
   * The filters to apply to the aircraft data
   */
  filters?: LiveFilters;
}

/**
 * The default RadarOptions
 *   FAA: true,
 *   FLARM: true,
 *   MLAT: true,
 *   ADSB: true,
 *   satellite: true,
 *   inAir: true,
 *   onGround: false,
 *   inactive: false,
 *   gliders: false,
 *   estimatedPositions: true,
 */
export const defaultRadarOptions: RadarOptions = {
  FAA: true,
  FLARM: true,
  MLAT: true,
  ADSB: true,
  satellite: true,
  inAir: true,
  onGround: false,
  inactive: false,
  gliders: false,
  estimatedPositions: true,
};
