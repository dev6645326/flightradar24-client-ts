export interface AirportDetailResponse {
  result: Result;
  api: API;
}

export interface API {
  copyright: Copyright;
  legalNotice: string;
}

export type Copyright =
  'Copyright (c) 2014-2023 Flightradar24 AB. All rights reserved.';

export interface Result {
  request: Request;
  response: AirportDetailData;
}

export interface Request {
  callback: null;
  code: string;
  device: null;
  fleet: null;
  format: Format;
  limit: number;
  page: number;
  pk: null;
  plugin: Plugin[];
  pluginSetting: PluginSetting;
  token: null;
}

export type Format = 'json';

export type Plugin =
  | 'details'
  | 'runways'
  | 'scheduledRoutesStatistics'
  | 'weather'
  | 'schedule';

export interface PluginSetting {
  schedule: PluginSettingSchedule;
  satelliteImage: SatelliteImage;
}

export interface SatelliteImage {
  scale: number;
}

export interface PluginSettingSchedule {
  mode: null;
  timestamp: number;
}

export interface AirportDetailData {
  airport: ResponseAirport;
  airlines: Airlines;
  aircraftImages: AircraftImage[] | null;
}

export interface AircraftImage {
  registration: Registration;
  images: Images | null;
}

export interface Images {
  thumbnails: Large[];
  medium?: Large[];
  large?: Large[];
  sideview?: string;
}

export interface Large {
  src: string;
  link: string;
  copyright: string;
  source: Source;
}

export type Source = 'Jetphotos.com' | 'Flightradar24';

export type Registration = number | string;

export interface Airlines {
  codeshare: { [key: string]: Codeshare } | null;
}

export interface Codeshare {
  name: null | string;
  code: CodeClass;
}

export interface CodeClass {
  iata: null | string;
  icao: null | string;
}

export interface ResponseAirport {
  pluginData: PluginData;
}

export interface PluginData {
  details: Details;
  schedule: PluginDataSchedule;
  weather: Weather | null;
  aircraftCount: AircraftCount;
  runways: Runway[];
  scheduledRoutesStatistics: ScheduledRoutesStatistics;
}

export interface AircraftCount {
  ground: number;
  onGround: OnGround;
}

export interface OnGround {
  visible: number;
  total: number;
}

export interface Details {
  name: string;
  code: CodeClass;
  delayIndex: DelayIndex;
  stats: Stats | null;
  position: Position;
  timezone: Timezone;
  url: URL;
  airportImages: Images | null;
  visible: boolean;
}

export interface DelayIndex {
  arrivals: number | null;
  departures: number | null;
}

export interface Position {
  latitude: number;
  longitude: number;
  elevation?: number;
  country: PositionCountry;
  region: Region;
}

export interface PositionCountry {
  name: string;
  code: string;
  id: number;
}

export interface Region {
  city: string;
}

export interface Stats {
  arrivals: DeparturesClass;
  departures: DeparturesClass;
}

export interface DeparturesClass {
  delayIndex: number;
  delayAvg: number;
  percentage: ArrivalsPercentage;
  recent: Recent;
  today: Day;
  yesterday: Day;
  tomorrow: Tomorrow;
}

export interface ArrivalsPercentage {
  delayed: number;
  canceled: number;
  trend: Trend;
}

export type Trend = 'up' | 'down';

export interface Recent {
  delayIndex: number;
  delayAvg: number;
  percentage: ArrivalsPercentage;
  quantity: Quantity;
}

export interface Quantity {
  onTime: number;
  delayed: number;
  canceled: number;
}

export interface Day {
  percentage: TodayPercentage;
  quantity: Quantity;
}

export interface TodayPercentage {
  delayed: number;
  canceled: number;
}

export interface Tomorrow {
  percentage: QuantityClass;
  quantity: QuantityClass;
}

export interface QuantityClass {
  canceled: number;
}

export interface Timezone {
  name: string;
  offset: number;
  abbr: string;
  abbrName: AbbrName | null;
  isDst: boolean;
}

export type AbbrName =
  | 'Eastern Daylight Time'
  | 'Central European Summer Time'
  | 'British Summer Time'
  | 'Pacific Daylight Time'
  | 'Eastern Standard Time'
  | 'Irish Standard Time'
  | 'Western European Summer Time'
  | 'Alaska Daylight Time'
  | 'Atlantic Standard Time'
  | 'Central Daylight Time'
  | 'Israel Daylight Time'
  | 'Central Standard Time'
  | 'Eastern European Summer Time'
  | 'Central European Time'
  | 'East Africa Time'
  | 'India Standard Time'
  | 'Greenwich Mean Time'
  | 'China Standard Time'
  | 'Korea Standard Time'
  | 'Hong Kong Time'
  | 'Mountain Daylight Time'
  | 'Japan Standard Time'
  | 'Mountain Standard Time'
  | 'South Africa Standard Time'
  | 'West Africa Time'
  | 'Central Africa Time'
  | 'Hawaii Standard Time';

export interface URL {
  homepage: null | string;
  webcam: null | string;
  wikipedia: string;
}

export interface Runway {
  name: string;
  length: Length;
  surface: Surface;
}

export interface Length {
  m: number;
  ft: number;
}

export interface Surface {
  code: CodeEnum;
  name: Name;
}

export type CodeEnum = 'CONC' | 'ASPH' | 'COAS' | 'MACA' | 'BITU';

export type Name =
  | 'Concrete'
  | 'Asphalt'
  | 'Unknown'
  | 'Macadam'
  | 'Bituminous';

export interface PluginDataSchedule {
  arrivals: ScheduleArrivals;
  departures: Departures;
  ground: Ground;
}

export interface ScheduleArrivals {
  item: Item;
  page: Page;
  timestamp: number;
  data: ArrivalsDatum[];
}

export interface ArrivalsDatum {
  flight: PurpleFlight;
}

export interface PurpleFlight {
  identification: Identification;
  status: FlightStatus;
  aircraft: PurpleAircraft | null;
  owner: Owner | null;
  airline: Airline | null;
  airport: PurpleAirport;
  time: Time;
}

export interface PurpleAircraft {
  model: Model;
  registration: string;
  country?: AircraftCountry;
  hex: string;
  restricted?: boolean;
  serialNo: SerialNo | null;
  age?: Age;
  availability?: Availability;
  images?: null;
}

export interface Age {
  availability: boolean;
}

export interface Availability {
  serialNo: boolean;
  age: boolean;
}

export interface AircraftCountry {
  id: number | null;
  name: null | string;
  alpha2: null | string;
  alpha3: null | string;
}

export interface Model {
  code: string;
  text: null | string;
}

export type SerialNo = '' | 'N/A';

export interface Airline {
  name: string;
  code?: CodeClass;
  short?: string;
}

export interface PurpleAirport {
  origin: Origin;
  destination: Destination;
  real: Real | null;
}

export interface Destination {
  timezone: Timezone;
  info: Info;
}

export interface Info {
  terminal: null | string;
  baggage: null | string;
  gate: null | string;
}

export interface Origin {
  code: CodeClass;
  timezone: Timezone;
  info: Info;
  name: string;
  position: Position;
  visible: boolean;
}

export interface Real {
  name: string;
  code: CodeClass;
  position: Position;
  timezone: Timezone;
  visible: boolean;
}

export interface Identification {
  id: null | string;
  row: number;
  number: number;
  callsign: null | string;
  codeshare: string[] | null;
}

export interface Number {
  default: null | string;
  alternative: null | string;
}

export interface Owner {
  name: null | string;
  code: CodeClass;
  logo: null | string;
}

export interface FlightStatus {
  live: boolean;
  text: string;
  icon: Icon | null;
  estimated: null;
  ambiguous: boolean;
  generic: Generic;
}

export interface Generic {
  status: GenericStatus;
  eventTime: EventTime;
}

export interface EventTime {
  utc: number | null;
  local: number | null;
}

export interface GenericStatus {
  text: StatusText;
  type: Type;
  color: Icon;
  diverted: null | string;
}

export type Icon = 'green' | 'gray' | 'red' | 'yellow';

export type StatusText =
  | 'estimated'
  | 'scheduled'
  | 'canceled'
  | 'landed'
  | 'delayed'
  | 'diverted'
  | 'departed';

export type Type = 'arrival' | 'departure';

export interface Time {
  scheduled: Estimated;
  real: Estimated;
  estimated: Estimated;
  other: Other;
}

export interface Estimated {
  departure: number | null;
  arrival: number | null;
}

export interface Other {
  eta: number | null;
  duration: number | null;
}

export interface Item {
  current: number;
  total: number;
  limit?: number;
}

export interface Page {
  current?: number;
  total: number;
}

export interface Departures {
  item: Item;
  page: Page;
  timestamp: number;
  data: DeparturesDatum[];
}

export interface DeparturesDatum {
  flight: FluffyFlight;
}

export interface FluffyFlight {
  identification: Identification;
  status: FlightStatus;
  aircraft: PurpleAircraft | null;
  owner: Owner | null;
  airline: Airline | null;
  airport: FluffyAirport;
  time: Time;
}

export interface FluffyAirport {
  origin: Destination;
  destination: Origin;
  real: null;
}

export interface Ground {
  item: Item;
  page: Page;
  timestamp: number;
  data: GroundDatum[];
}

export interface GroundDatum {
  flight: TentacledFlight;
}

export interface TentacledFlight {
  identification: Identification;
  aircraft: FluffyAircraft;
  owner: Owner;
  airline: Airline | null;
}

export interface FluffyAircraft {
  model: Model;
  registration: string;
  country: AircraftCountry;
  hex: string;
  restricted: boolean;
  serialNo: SerialNo;
  age: Age;
  availability: Availability;
  onGroundUpdate: number;
  hoursDiff: number;
  timeDiff: number;
}

export interface ScheduledRoutesStatistics {
  totalFlights: number;
  topRoute: TopRoute | null;
  airportsServed: number;
  countriesServed: number;
}

export interface TopRoute {
  from: string;
  to: string;
  count: number;
}

export interface Weather {
  metar: string;
  time: number;
  qnh: number;
  dewpoint: Dewpoint;
  humidity: number;
  pressure: Pressure;
  sky: Sky;
  flight: WeatherFlight;
  wind: Wind;
  temp: Dewpoint;
  elevation: Length;
  cached: number;
}

export interface Dewpoint {
  celsius: number;
  fahrenheit: number;
}

export interface WeatherFlight {
  category: null | string;
}

export interface Pressure {
  hg: number;
  hpa: number;
}

export interface Sky {
  condition: Condition;
  visibility: Visibility;
}

export interface Condition {
  text: ConditionText;
}

export type ConditionText = 'Rain' | 'Overcast' | 'Cloudy' | 'Clear';

export interface Visibility {
  km: number;
  mi: number;
  nmi: number;
}

export interface Wind {
  direction: Direction;
  speed: Speed;
}

export interface Direction {
  degree: number;
  text: string;
}

export interface Speed {
  kmh: number;
  kts: number;
  mph: number;
  text: SpeedText;
}

export type SpeedText = 'Mod' | 'Strong' | 'Calm';

export interface AirportData {
  name: string;
  iata: string;
  icao: string;
  lat: number;
  lon: number;
  country: string;
  alt: number;
}
